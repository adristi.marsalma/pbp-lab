from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='lab-4'),
    path('add-note/', add_note),
    path('note-list/', note_list),

]