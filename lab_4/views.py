from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    forms = NoteForm(request.POST)
    if forms.is_valid():
        forms.save()
        return redirect('lab-4')
    context = {'forms':forms}
    return render(request, 'lab4_form.html', context)

@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_note_list.html', response)
