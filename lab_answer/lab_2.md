Adristi Marsalma Nectarine Amaranthi
2006483113

Jawaban lab 2

1. Apakah perbedaan antara JSON dan XML?
Keduanya sama-sama digunakan untuk format pertukaran data. Bedanya, XML adalah markup 
language, sedangkan JSON adalah format yang ditulis dalam JavaScript. Berikut penjelasan 
lebih lengkap terkait perbedaan XML dan JSON:

XML 
Data disimpan sebagai tree             
Transmisi data lebih lambat            
Lebih rumit                             
Tidak support array                     
Support beragam tipe data

JSON
Data disimpan dalam bentuk map
Transmisi data lebih cepat
Lebih sederhana
Support array

sumber: https://www.monitorteknologi.com/perbedaan-json-dan-xml/

2. Apakah perbedaan antara HTML dan XML?
Keduanya merupakan markup language yang berbeda. Perbedaan mendasar adalah perbedaan fungsi. 
HTML berfungsi untuk mengatur tampilan dari web, meliputi formatting dan mengatur layout. 
XML berfungsi untuk mengatur struktur dan isi data yang akan ditampilkan.
Berikut penjelasan lebih lengkap terkait perbedaan XML dan HTML:

XML				 
Berfokus pada penyajian data    
Case insensitive   
Tidak support namespace
Tidak wajib disertai closing tags  
Tag terbatas

HTML
Berfokus pada transfer data	            
Case sensitive			                
Support namespace		                
Wajib disertai closing tags	            
Tag XML bisa dikembangkan	            

sumber: https://blogs.masterweb.com/perbedaan-xml-dan-html/
