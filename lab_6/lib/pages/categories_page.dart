import 'package:flutter/material.dart';
import 'package:lab_6/widgets/list_content.dart';

class CategoriesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        height: 119,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RichText(
              text: TextSpan(
                text: 'Apotek',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.yellow,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Daring',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 4,
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.only(
          left: 24,
          right: 24,
        ),
        child: Column(
          children: [
            Container(
              height: 60,
              padding: const EdgeInsets.only(
                left: 20,
                top: 8,
              ),
              margin: const EdgeInsets.symmetric(
                horizontal: 25,
                vertical: 25,
              ),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: (Colors.grey[350]!),
                    blurRadius: 20.0,
                    offset: const Offset(0, 10),
                  ),
                ],
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              ),
              child: TextFormField(
                decoration: const InputDecoration(
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.yellow,
                    size: 20,
                  ),
                  border: InputBorder.none,
                  hintText: 'Cari produk...',
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ListContent(
                  imageback: ('assets/counterpain.jpg'),
                  productLink:
                      'https://www.k24klik.com/p/counterpain-cr-15g-1028',
                ),
                ListContent(
                  imageback: 'assets/panadol.jpg',
                  productLink:
                      'https://www.k24klik.com/p/panadol-biru-tab-100s-414',
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Counterpain',
                          style: TextStyle(
                            color: Color(0xff242D3A),
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          '\Mengatasi nyeri sendi',
                          style: TextStyle(
                            color: Color(0xff242D3A),
                            fontSize: 12,
                            fontWeight: FontWeight.w200,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 12,
                    right: 90,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Panadol',
                        style: TextStyle(
                          color: Color(0xff242D3A),
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        '\Mengatasi pusing',
                        style: TextStyle(
                          color: Color(0xff242D3A),
                          fontSize: 12,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListContent(
                    imageback: ('assets/decolsin.jpg'),
                    productLink:
                        "https://www.k24klik.com/p/decolsin-cap-str-4's-288",
                  ),
                  ListContent(
                    imageback: ('assets/obh.jpg'),
                    productLink:
                        'https://www.k24klik.com/p/obh-combi-dewasa-batuk-flu-rasa-jahe-100ml-9336',
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Decolsin',
                          style: TextStyle(
                            color: Color(0xff242D3A),
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          '\Mengatasi gejala flu',
                          style: TextStyle(
                            color: Color(0xff242D3A),
                            fontSize: 12,
                            fontWeight: FontWeight.w200,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 12,
                    right: 90,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'OBH',
                        style: TextStyle(
                          color: Color(0xff242D3A),
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        '\Mengatasi batuk',
                        style: TextStyle(
                          color: Color(0xff242D3A),
                          fontSize: 12,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(title: Text('Lindungi Peduli')),
      body: ListView(
        children: [
          header(),
          content(),
        ],
      ),
    );
  }
}
