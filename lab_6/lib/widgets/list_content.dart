import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ListContent extends StatelessWidget {
  String? imageback;
  String? productLink;

  ListContent({
    this.imageback,
    this.productLink,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 160,
      height: 190,
      decoration: BoxDecoration(
        color: Color(0xffE5E5E5),
        borderRadius: BorderRadius.circular(16),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Stack(
          children: [
            Image.asset(
              imageback!,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Padding(
                padding: const EdgeInsets.only(
                  top: 150,
                  left: 75,
                ),
                child: SizedBox(
                  width: 75,
                  height: 30,
                  child: ElevatedButton(
                    child: Text(
                      'Beli sekarang',
                      style: TextStyle(
                        fontSize: 10,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () async {
                      await openUrl(productLink!);
                    },
                  ),
                )),
          ],
        ),
      ),
    );
  }
}

Future<void> openUrl(String url,
    {bool forceWebView = false, bool enableJavaScript = false}) async {
  await launch(url);
}
