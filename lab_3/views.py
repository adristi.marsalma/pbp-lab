from django.shortcuts import redirect, render
from django.http import response
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    forms = FriendForm(request.POST)
    if forms.is_valid():
        forms.save()
        return redirect('lab-3')
    context = {'forms':forms}
    return render(request, 'lab3_form.html', context)
