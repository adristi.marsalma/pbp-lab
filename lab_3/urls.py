from django.urls import path
from .views import index, add_friend
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', index, name='lab-3'),
    path('add/', add_friend),
    #path('admin/login/', auth_views.LoginView.as_view()),
]